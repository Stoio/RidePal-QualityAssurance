In order to run the tests the following components should be prepared:

1. Node.js should be installed.

2. RidePal project should be running on local host. Its database should be reruned before test execution.

3. "newman" and "newman-reporter-htmlextra" should be installed. 
   Files named "Install newman" and "Install newman-reporter-htmlextra" are containing the scripts for it.
   They can be ran in the order they are mentioned to automatically install these features or they can be installed manually.

4. The tests are executed by running "RunPostmanTests.batch".

5. Test report can be found after test execution in the "newman" folder.

