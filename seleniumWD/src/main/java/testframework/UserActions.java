package testframework;

import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;

import java.util.List;

public class UserActions {
	final WebDriver driver;

	public UserActions() {
		this.driver = Utils.getWebDriver();
	}

	public static void loadBrowser() {
		Utils.getWebDriver().get(Utils.getConfigPropertyByKey("base.url"));
	}

	public static void quitDriver(){
		Utils.tearDownWebDriver();
	}

	public void clickElement(String key){
		WebElement element =driver.findElement(By.xpath(Utils.getUIMappingByKey(key)));
		element.click();
		Utils.LOG.info("Clicking on element " + key);
	}

	public void clickElementByCssSelector(String key){
		WebElement element =driver.findElement(By.cssSelector(Utils.getUIMappingByKey(key)));
		element.click();
		Utils.LOG.info("Clicking on element " + key);
	}

	public void clickElementJS (String locator){
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator)));
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		Utils.LOG.info("Clicking on element " + locator);
	}

	public void typeValueInField(String value, String field){
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(field)));
		element.sendKeys(value);
		Utils.LOG.info("Typing {" + value +"} in the field: {" + field + "}");
	}

	public void typeTextJS (String text, String field) {
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(field)));
		((JavascriptExecutor)driver).executeScript("arguments[0].value = arguments[1];", element, text);
		Utils.LOG.info("Typing: {" + text + "} in the field: {" + field + "}");
	}

    public void sendKeys(Keys key){
		Actions action = new Actions(driver);
		action.sendKeys(key).perform();
		Utils.LOG.info(key+" pressed");
	}

	public void sendKeysAtLocation(String location, Keys keys){
		WebElement type = driver.findElement(By.xpath(Utils.getUIMappingByKey(location)));
		type.sendKeys(keys);
	}

	//############# WAITS #########

	public void waitForElementVisible(String locator, int seconds){
		WebElement element= driver.findElement(By.xpath(Utils.getUIMappingByKey(locator)));
		WebDriverWait wait= new WebDriverWait(driver,seconds);
		wait.until(ExpectedConditions.visibilityOf(element));
	}

	public void waitForElementClickable (String locator, int seconds){
		WebDriverWait wait= new WebDriverWait(driver,seconds);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Utils.getUIMappingByKey(locator))));
	}

	public void fixedWait (int milliseconds){
		try {
			Thread.sleep(milliseconds);} catch (Exception next) {
		}
	}

	public void waitForElementPresent(String locator, int count) {

		while (count-- > 0) {
			try {
				WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator)));
				waitForElementClickable(locator, count);
//				Utils.LOG.info("Count numbers: " + count);
				return;
			} catch (NoSuchElementException ex) {
				try {
					Thread.sleep(500);} catch (Exception next) {
				}
			}
		}
	}

	//############# ASSERTS #########  ?????

	public void assertElementPresent(String locator){

		Assert.assertNotNull(driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))));
		Utils.LOG.info("Element is present: " + locator);
	}
	public void assertElementsEqual(String expected, String actual){

		Assert.assertEquals(expected,driver.findElement(By.xpath(Utils.getUIMappingByKey(actual))).getText());
		Utils.LOG.info(expected + " equals "+ actual);
	}
	public void assertBoolsEqual(boolean expected, boolean actual){
		Assert.assertEquals(expected,actual);
		Utils.LOG.info(expected + " equals "+ actual);
	}

	// ##### Log In with the base User/Pass from config.properties####

	public void logInWithTestUser (String email, String password) {

		clickElement("loginMainPage.Button");
		typeTextJS(Utils.getConfigPropertyByKey(email),"email.Input");
		typeTextJS(Utils.getConfigPropertyByKey(password),"password.Input");
		clickElementJS("login.Button");
	}

	// ##### Log In with user email and password ####

	public void login (String email, String password) {

		typeTextJS(email,"email.Input");
		typeTextJS(password,"password.Input");
		clickElementJS("login.Button");
	}

	public void logOut (){
		clickElement("image.RidePal");
		waitForElementClickable("profile.Image",4);
		clickElement("profile.Image");
		waitForElementPresent("logout.Button",4);
		fixedWait(2000);
		clickElement("logout.Button");
		}

	// ##### Register new user with fixed country (Bulgaria) ####

	public void registration(String firstName, String lastName, String email, String password) {

		waitForElementPresent("registerMainPage.Button",4);
		clickElement("registerMainPage.Button");
		waitForElementPresent("firstName.Input",4);
		typeValueInField(firstName,"firstName.Input");
		typeValueInField(lastName,"lastName.Input");
		typeValueInField(email, "email.Input");
		typeValueInField(password,"password.Input");
		typeValueInField(password,"passwordConfirmation.Input");
		waitForElementVisible("country.Button",3);
		clickElement("country.Button");
		clickElement("country.Button2");
		clickElement("register.Button");
	}

	//############# BROWSER FUNCTIONS #########

	public void refreshPage (){
		driver.navigate().refresh();
		Utils.LOG.info("Page refresh");
	}

	public void goToHomePage () {
//		driver.navigate().to(Utils.getConfigPropertyByKey("base.url"));
		driver.get(Utils.getConfigPropertyByKey("base.url"));
		Utils.LOG.info("back to Home page");
	}

	//############ RANDOM STRINGS GENERATOR #################

	public String getRandomString(int n)
	{
		// chose a Character random from this String
		String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
				+ "0123456789"
				+ "abcdefghijklmnopqrstuvxyz";
		// create StringBuffer size of AlphaNumericString
		StringBuilder sb = new StringBuilder(n);
		for (int i = 0; i < n; i++) {
			// generate a random number between
			// 0 to AlphaNumericString variable length
			int index
					= (int)(AlphaNumericString.length()
					* Math.random());
			// add Character one by one in end of sb
			sb.append(AlphaNumericString
					.charAt(index));
		}
		return sb.toString();
	}

	//################## SCROLLERS ######################

	public void scrollByVisibleElement(String locator) {
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator)));
		JavascriptExecutor js = ((JavascriptExecutor) driver);
		js.executeScript("arguments[0].scrollIntoView()", element);
	}

	public void goToElement(String locator){
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator)));
		Actions actns = new Actions(driver);
		actns.moveToElement(element);
		actns.perform();
		Utils.LOG.info(locator+ " found");
	}

//################### GETTERS #######################
	public String getElementStringValue (String locator){
		WebElement element =driver.findElement(By.xpath(Utils.getUIMappingByKey(locator)));
		return element.getText();
	}
	public String getElementStringValueByWebElement (WebElement element){
		return element.getText();
	}
	public List getListOfElements(String locators){
		List<WebElement> allElements = driver.findElements(By.xpath(Utils.getUIMappingByKey(locators)));
		return allElements;
	}

	public boolean listContainsElement (String locators, String expected){
		boolean contains=false;
		for (int i = 0; i < getListOfElements(locators).size(); i++) {
			if(getElementStringValueByWebElement((WebElement)(getListOfElements(locators).get(i))).equals(expected)) {
				contains=true;
			}
		}
		Utils.LOG.info("Searching for " + expected + " in the list.");
		return contains;
	}

//##################### COORDINATE METHODS #######################
	public void mouseClickToCoordinates(int x, int y) {
		new Actions(driver).moveByOffset(x,y).click().build().perform();
	}

	public void contextClickToCoordinates(int x, int y) {
		new Actions(driver).moveByOffset(x,y).contextClick().build().perform();
	}

	public void resetMouseCoordinates() {
		new Actions(driver).moveByOffset(0, 0).perform();
	}
	//################### CLEAR TEXT ########################
	public void clearText(String locator) {
		WebElement toClear = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator)));
		toClear.sendKeys(Keys.CONTROL + "a");
		toClear.sendKeys(Keys.DELETE);
	}
}
