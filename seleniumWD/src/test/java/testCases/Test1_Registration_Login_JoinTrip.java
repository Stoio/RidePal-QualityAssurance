package testCases;

import org.junit.Test;

public class Test1_Registration_Login_JoinTrip extends BaseTest {
    private String USER_DENI ="Deniza Topalova";
    private String USER_TEST ="Test Testov";

    @Test
    public void registerNewUser () {

		registration(firstName);
        actions.assertElementPresent("login.Button");
    }

    @Test
    public void registerAndLogin () {

       registration(firstName);
       login(email);
       actions.waitForElementPresent("image.RidePal",5);
       actions.assertElementPresent("image.RidePal");
    }

    @Test
    public void registerAndSendTripRequest(){

        registration(firstName);
        login(email);
        sendTripRequest(USER_TEST);
        actions.waitForElementPresent("pendingTrip.Button",5);
        actions.assertElementPresent("pendingTrip.Button");
    }

    @Test
    public void acceptReceivedTripRequest() {

        registration(firstName);
        login(email);
        sendTripRequest(USER_DENI);
        actions.logOut();
        actions.logInWithTestUser("deni.email","deni.pass");
        acceptTripRequest();
        actions.assertElementPresent("friendship.Sign");
        actions.clickElement("createdPost.OKBtn");
    }

    @Test
    public void rejectReceivedTripRequest() {
        String localFirstName = firstName;
        registration(localFirstName);
        login(email);
        sendTripRequest(USER_DENI);
        actions.logOut();
        actions.logInWithTestUser("deni.email","deni.pass");
        rejectTripRequest();
        boolean actual = actions.listContainsElement("tripReq.Users.List",firstName +" "+ lastName);
        actions.assertBoolsEqual(false,actual);
    }

    public void registration(String firstName) {

        actions.waitForElementPresent("registerMainPage.Button",4);
        actions.clickElement("registerMainPage.Button");
        actions.waitForElementPresent("firstName.Input",4);
        actions.typeValueInField(firstName,"firstName.Input");
        actions.typeValueInField(lastName,"lastName.Input");
        actions.typeValueInField(email, "email.Input");
        actions.typeValueInField(password,"password.Input");
        actions.typeValueInField(password,"passwordConfirmation.Input");
        actions.waitForElementVisible("country.Button",3);
        actions.clickElement("country.Button");
        actions.clickElement("country.Button2");
        actions.clickElement("register.Button");
    }

    public void login (String email) {

        actions.typeTextJS(email,"email.Input");
        actions.typeTextJS(password,"password.Input");
        actions.clickElementJS("login.Button");
    }

    private void sendTripRequest(String user){
        actions.fixedWait(sleepMilliseconds);
        actions.typeValueInField(user,"search.Field");
        actions.fixedWait(sleepMilliseconds);
        actions.clickElement("suggested.Profile");
        actions.fixedWait(sleepMilliseconds);
        actions.clickElementByCssSelector("joinTrip.ButtonCss");
    }

    private void acceptTripRequest(){
        actions.fixedWait(sleepMilliseconds);
        actions.clickElement("deni.Profile");
        actions.fixedWait(sleepMilliseconds);
        actions.clickElement("tripRequests.Btn");
        actions.fixedWait(sleepMilliseconds);
        actions.clickElement("tripRequest.Approve");
        actions.fixedWait(sleepMilliseconds);
    }

    private void rejectTripRequest(){
        actions.fixedWait(sleepMilliseconds);
        actions.clickElement("deni.Profile");
        actions.fixedWait(sleepMilliseconds);
        actions.clickElement("tripRequests.Btn");
        actions.fixedWait(sleepMilliseconds);
        actions.clickElement("tripRequest.Reject");
        actions.fixedWait(sleepMilliseconds);
    }
}


