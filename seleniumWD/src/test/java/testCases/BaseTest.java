package testCases;

import org.junit.After;
import org.junit.Before;
import testframework.UserActions;

public class BaseTest {

	final int sleepMilliseconds = 2000;
	UserActions actions = new UserActions();
	String firstName = "User_"+ actions.getRandomString(3);
	String lastName = "User";
	String email = firstName+ "@abv.bg";
	String password = "user";

	@Before
	public  void setUp(){
		UserActions.loadBrowser();
	}

	@After
	public void tearDown(){
		UserActions.quitDriver();
	}
}


