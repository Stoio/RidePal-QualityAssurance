package testCases;

import org.junit.Test;
import org.openqa.selenium.Keys;

public class Test2_CreatePost_EditPost extends BaseTest {

    private String postMsg = "Test post text";
    private String fromLoc = "Sofia Bulgaria";
    private String toLoc = "Varna Bulgaria";

    private String editMsg = "Edited Post";
    private String editFromLoc = "Silistra, Bulgaria";
    private String editToLoc = "Burgas, Bulgaria";
    private int coordinateX = 500;
    private int coordinateY = 0;

    @Test
    public void post (){

        actions.logInWithTestUser("test.email","test.pass");

        actions.fixedWait(sleepMilliseconds);
        actions.typeValueInField(postMsg,"post.Field");
        actions.fixedWait(sleepMilliseconds);
        actions.clickElement("post.Field");
        actions.waitForElementPresent("location.Button",3);
        actions.clickElementJS("location.Button");
     // In Location panel:
        ///////// From /////////
        actions.typeTextJS(fromLoc,"location.Input");
        actions.fixedWait(sleepMilliseconds);
        actions.sendKeysAtLocation("location.Input", Keys.ENTER);
        actions.fixedWait(sleepMilliseconds);      // MUST WAIT TILL LOCATION TRIGGERS BEFORE CLICK !!!!
        actions.clickElement("from.Button");
        ///////// To /////////
        actions.typeTextJS(toLoc,"location.Input");
        actions.sendKeysAtLocation("location.Input", Keys.ENTER);
        actions.fixedWait(sleepMilliseconds);
        actions.clickElement("to.Button");
        ///////// Time /////////
        actions.clickElement("time.Button");
        actions.waitForElementPresent("clockUpArrow.Button",4);
        actions.clickElement("clockUpArrow.Button");
        actions.clickElement("clockUpArrow.Button");
        actions.resetMouseCoordinates();
        actions.mouseClickToCoordinates(coordinateX,coordinateY);
        actions.fixedWait(sleepMilliseconds);
        actions.clickElement("calcRoute.Button");
        actions.fixedWait(sleepMilliseconds);
        actions.clickElement("confirmRoute.Button");
        actions.fixedWait(sleepMilliseconds);
        actions.clickElement("share.Button");
        actions.fixedWait(sleepMilliseconds);
        actions.clickElement("createdPost.OKBtn");
        actions.scrollByVisibleElement("postBody.Text");
        ///////// Assert /////////
        actions.assertElementsEqual(postMsg,"postBody.Text");

    }
    @Test
    public void editPost (){

        actions.logInWithTestUser("test.email","test.pass");

        actions.waitForElementClickable("editPost.Btn",6);
        actions.clickElement("editPost.Btn");
        actions.fixedWait(sleepMilliseconds);

        ///////// Update Body /////////
        actions.goToElement("updatePost.Body");
        actions.clearText("updatePost.Body");
        actions.typeValueInField(editMsg,"updatePost.Body");
        actions.goToElement("editLocation.Input");

        ///////// From /////////
        actions.typeTextJS(editFromLoc,"editLocation.Input");
        actions.fixedWait(sleepMilliseconds);
        actions.sendKeysAtLocation("editLocation.Input", Keys.ENTER);
        actions.fixedWait(sleepMilliseconds);
        actions.clickElement("fromEdit.Button");

        ///////// To /////////
        actions.typeTextJS(editToLoc,"editLocation.Input");
        actions.sendKeysAtLocation("editLocation.Input", Keys.ENTER);
        actions.fixedWait(sleepMilliseconds);
        actions.clickElement("toEdit.Button");

        ///////// Time /////////
        actions.clickElement("timeEdit.Button");
        actions.waitForElementPresent("clockUpArrow.Button",4);
        actions.clickElement("clockUpArrow.Button");
        actions.clickElement("clockUpArrow.Button");
        actions.fixedWait(sleepMilliseconds);
        actions.resetMouseCoordinates();
        actions.mouseClickToCoordinates(coordinateX,coordinateY);
        actions.fixedWait(sleepMilliseconds);

        ///////// Confirmation /////////
        actions.clickElement("calcRouteEdit.Button");
        actions.fixedWait(sleepMilliseconds);
        actions.sendKeys(Keys.ARROW_DOWN);
        actions.sendKeys(Keys.ARROW_DOWN);
        actions.scrollByVisibleElement("shareEdit.Btn");
        actions.fixedWait(sleepMilliseconds);
        actions.clickElement("shareEdit.Btn");
        actions.fixedWait(sleepMilliseconds);
        actions.clickElement("createdPost.OKBtn");
        actions.refreshPage();
        actions.fixedWait(sleepMilliseconds);
        ///////// Assert /////////
        actions.assertElementsEqual(editMsg,"postBody.Text");

    }
}
