package testCases;

import org.junit.Test;
import org.openqa.selenium.Keys;

public class Test3_LikeAndUnlikePost_WriteComment extends BaseTest {

    private final String TEST_WHOLE_NAME = "Test Testov";
    private final int RANDOM_STRING_LENGTH=5;
    private final String COMMENT = actions.getRandomString(RANDOM_STRING_LENGTH);

    @Test
    public void likeAPost(){
        actions.registration(firstName, lastName, email, password);
        actions.login(email, password);
        likePost();
    }

    @Test
    public void unlikeAPost(){
        actions.logInWithTestUser("test.email","test.pass");
        likePost();
        unlikePost();
    }

    @Test
    public void writeAComment(){

        actions.logInWithTestUser("test.email","test.pass");
        submitComment();
    }

    private void likePost(){

        actions.fixedWait(sleepMilliseconds);
        int expectedLikesValue = Integer.parseInt(actions.getElementStringValue("likes.Counter"));
        actions.clickElement("like.Button");
        actions.waitForElementPresent("unLike.Button",4);
        actions.assertElementPresent("unLike.Button");
        actions.refreshPage();
        actions.fixedWait(sleepMilliseconds);
        actions.assertElementsEqual(Integer.toString(expectedLikesValue + 1),"likes.Counter");
    }

    private void unlikePost(){

        actions.fixedWait(sleepMilliseconds);
        int expectedLikesValue2 = Integer.parseInt(actions.getElementStringValue("likes.Counter"));
        actions.clickElement("like.Button");
        actions.refreshPage();
        actions.waitForElementPresent("like.Button",4);
        actions.assertElementsEqual(Integer.toString(expectedLikesValue2-1),"likes.Counter");
    }

    private void submitComment(){

        actions.fixedWait(sleepMilliseconds);
//        actions.goToElement("writeComment.Field");
        actions.scrollByVisibleElement("writeComment.Field");
        actions.fixedWait(sleepMilliseconds);
        actions.clickElement("writeComment.Field");
        actions.typeValueInField(COMMENT,"writeComment.Field");
        actions.sendKeys(Keys.ENTER);
        actions.fixedWait(sleepMilliseconds);
        actions.assertElementsEqual(TEST_WHOLE_NAME + " " + COMMENT,"comment.Body");
    }
}
