package testCases;

import org.junit.Test;

public class Test4_Send_Accept_Reject_FriendRequest extends BaseTest {

    private final String FRIEND_REMOVED_MESSAGE="Removed";

    String firstName = actions.getRandomString(5);
    String lastName = actions.getRandomString(4);;
    String email = firstName + "@abv.bg";

    @Test
    public void sendFriendRequest(){

        String localFirstName = firstName;
        String localEmail = localFirstName + "@abv.bg";

        ////// Register new User /////
        actions.registration(localFirstName,lastName,localEmail,password);

        ////// Log in with TestUser abd send Friend Request to the new User/////
        actions.login("Test@abv.bg","test");
        sendFriendRequest(localFirstName);

        /////// Assert //////
        actions.assertElementPresent("friendReq.Pending");
    }
    @Test
    public void acceptReceivedFriendRequest(){

        String localFirstName = firstName;
        String localEmail = localFirstName + "@abv.bg";

        ////// Register new User /////
        actions.registration(localFirstName,localEmail,email,password);

        ////// Log in with TestUser /////
        actions.login("Test@abv.bg","test");

        ////// Send Friend Request to the new User/////
        sendFriendRequest(localFirstName);
        actions.logOut();

        ///// Log in with the new User and accept friend request
        actions.waitForElementPresent("loginMainPage.Button",5);
        actions.clickElement("loginMainPage.Button");
        actions.login(localEmail,password);
        actions.fixedWait(sleepMilliseconds);
        acceptFriendRequest();
        actions.fixedWait(sleepMilliseconds);

        /////// Assert //////
        actions.assertElementPresent("friendship.Sign");

    }
    @Test
    public void removeUserFromFriendList(){

        String localFirstName = firstName;
        String localEmail = localFirstName + "@abv.bg";

        ////// Register new User /////
        actions.registration(localFirstName,localEmail,email,password);

        ////// Log in with TestUser /////
        actions.login("Test@abv.bg","test");

        ////// Send Friend Request to the new User/////
        sendFriendRequest(localFirstName);
        actions.logOut();

        ///// Log in with the new User and accept TestUser request
        actions.waitForElementPresent("loginMainPage.Button",5);
        actions.clickElement("loginMainPage.Button");
        actions.login(localEmail,password);
        actions.fixedWait(sleepMilliseconds);
        acceptFriendRequest();

        ///// Remove TestUser from friend list
        actions.fixedWait(sleepMilliseconds);
        removeFriend();

        /////// Assert //////
        actions.assertElementsEqual(FRIEND_REMOVED_MESSAGE,"friendRemoved.Text");
        actions.clickElement("createdPost.OKBtn");
    }

    public void sendFriendRequest(String USER) {

        actions.fixedWait(sleepMilliseconds);
        actions.typeValueInField(USER, "search.Field");
        actions.waitForElementPresent("suggested.Profile", 6);
        actions.clickElement("suggested.Profile");
//        actions.refreshPage();
        actions.fixedWait(sleepMilliseconds);
        actions.clickElement("addFriend.Btn");
        actions.fixedWait(sleepMilliseconds);
        actions.clickElement("createdPost.OKBtn");
    }

    public void acceptFriendRequest() {

        actions.waitForElementPresent("profile.Image",4);
        actions.clickElement("profile.Image");
        actions.fixedWait(sleepMilliseconds);
        actions.clickElement("friendRequests.Btn");
        actions.fixedWait(sleepMilliseconds);
        actions.clickElement("friendRequest.Approve");
        actions.fixedWait(sleepMilliseconds);
        actions.assertElementPresent("friendship.Sign");
        actions.clickElement("createdPost.OKBtn");
    }

    public void removeFriend(){
        
        actions.clickElement("friends.List");
        actions.fixedWait(sleepMilliseconds);
        actions.clickElement("friendRemove.Btn");//
        actions.fixedWait(sleepMilliseconds);
        actions.clickElement("createdPost.OKBtn");
        actions.fixedWait(sleepMilliseconds);
    }
}
