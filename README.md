Workplace for QA 2019 **FINAL PROJECT**

# RidePal-QualityAssurance

`RidePal` is a web application for shared travelling.

RidePal repo: https://gitlab.com/kycenstein/ridepal



**Project members**

|       Name       	|     Role      	| Permission   	|
|:----------------:	|:-------------:	|:-------------:|
| Simeon Georev 	| QA    	        | Maintainer    |
| Stoyan Ivanov 	| QA             	| Maintainer    |
| Pavlina Koleva    | Telerik Academy 	|  Reporter    	|
| Vladimir Venkov   | Telerik Academy 	|  Reporter    	|
| Petya Petarcheva  | ScaleFocus    	|  Reporter    	|
| Deniza Topalova  	| RidePal - Dev 	|  Reporter    	|
| Krastan Stoyanov 	| RidePal-  Dev  	|  Reporter     |


**Test Plan:** https://docs.google.com/document/d/123sGeopG48ATpH63qalijIf_e03TpOHfAFVc23DGgok/edit?usp=sharing

**Test Cases:** https://docs.google.com/spreadsheets/d/1IDRtXzE7FB273a0EgNTixJbYtIqS2ZZJfE5sAsm_PT8/edit#gid=0

**Tasks Board:** https://trello.com/b/3gBb51sm/tasks-destribution

**Logged Issues:** https://gitlab.com/kycenstein/ridepal/issues?scope=all&utf8=%E2%9C%93&state=all

**Bug Report:** https://docs.google.com/document/d/1e0hvsw24Vhn_ECsnbXU39rOg8cifAXhOnHjzmQCCUSg/edit?usp=sharing

**Exploratory Testing:** https://docs.google.com/document/d/1-5cGiWDCWKiOyYxlL4JY3ky275s-JRp5HCN5KqI2Xps/edit?fbclid=IwAR2jhuZna7G0mf4Rfa0n-h5c21mLgo46vUFwo6Bi8aQTiJxwPBDLlJ23uy4