#  <ins> How to build RidePal application </ins>

*Prerequisites:*
-   MariaDB (DB) 
-   IntelliJ

## 1/  Download source from:

https://gitlab.com/kycenstein/ridepal


## 2/  Run ridePal from  **build.gradle**

## 3/ Connect to DB with
user: user <br>
password: password 

<pre><code>
CREATE USER 'user' IDENTIFIED BY 'password';
GRANT USAGE ON *.* TO 'user'@localhost IDENTIFIED BY 'password';
GRANT ALL privileges ON `RidePal`.* TO 'user'@localhost;authoritiesusers
</code></pre>

## 4/ Fill in DataBase     {Use the sripts from db folder - located in source} :
1/ create-tables-RidePal.sql <br>
2/ dump_countries-RidePal.sql <br>
3/ dump-database-RidePal.sql <br>

## 5/ Dell all data in "application.properties" (in IntelliJ) and fill in with:

<pre><code>
#Data Source Properties
spring.datasource.driverClassName=com.mysql.jdbc.Driver
spring.datasource.url=jdbc:mysql://localhost:3306/ridepal?useUnicode=true&characterEncoding=UTF-8&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC
spring.datasource.username = user
spring.datasource.password = password
##Data Source Properties
#spring.datasource.driverClassName=com.mysql.jdbc.Driver
#spring.datasource.url= ${DO_URL}
#spring.datasource.username = ${DO_USERNAME}
#spring.datasource.password = ${DO_PASSWORD}
##Data Source Properties
#spring.datasource.driverClassName=com.mysql.jdbc.Driver
#spring.datasource.url=jdbc:mysql://doadmin:j7gaj62b1c3tlw2y@ridepal-digiocean-do-user-6730887-0.db.ondigitalocean.com:25060/ridepal?ssl-mode=REQUIRED
#spring.datasource.username = doadmin
#spring.datasource.password =  j7gaj62b1c3tlw2y
#JPA Properties
#spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.MySQL5Dialect
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.MySQL5InnoDBDialect
spring.jpa.properties.hibernate.format_sql=TRUE
#spring.jpa.show-sql = true9
#spring.jpa.hibernate.ddl-auto=update
spring.servlet.multipart.enabled=true
spring.servlet.multipart.file-size-threshold=2KB
spring.servlet.multipart.max-file-size=200MB
spring.servlet.multipart.max-request-size=215MB
#ENCODING PROPERTIES TODO: CHECK IT!
#server.tomcat.uri-encoding=UTF-8
#spring.http.encoding.charset=UTF-8
#spring.flyway.encoding=UTF-8
</code></pre>

## 6/ Run the App from IntelliJ  
## 7/ Open ridepal in your browser - localhost  (http://localhost:8080)



